# Algorithm and Programming (COMP 6047)

## Manual Tokeniser / Assignment 3

### Description
The purpose of this assignment is to familiarise students with strings in C Programming Language. The program acts as a `strtok()`, but in a different way. It only tokenise the space only.

### Compiling the Source Code
To compile the source code, we can simply execute the following command:

`gcc *.c`

The `*.c` will compile everything on current folder that has extension `.c`. Since there are only 1 source code, it is easier to use `*.c`

### How to run
Simply open the binary file that has been produced by the `gcc` compiler command. The default name is `a.out`. After the program has started, it will wait for the user input. Try to type `Hello world` and then press enter. It will separate the words based on spaces.

### Known bugs
There are no indicators that the program is ready to receive input, which is a bad practise. And also the program use gets() which is unsafe.
