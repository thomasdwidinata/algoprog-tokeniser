#include <stdio.h>
#include <string.h>
// Thomas Dwi Dinata - 1901498151
// TOKENIZER v1.0


void main()
{
	// Declaring 500 words of ASCII characters, Loop Indicator, ASCII Number, the string length
	char string[500]; // 500 words of ASCII Chars
	int a, b; // 'a' is for Loop Counter, 'b' is for saving the ASCII Code to Decimal
	int len; // strlen needs this variable to save how many chars that in the array

	// Program Start
	gets(string); // GET String input from user and save it to array 'string'
	len=strlen(string); // Counting the string length
	
	// Loop Start
	a=0;
	for(a=0;a<len-1;++a)
	{
		for(;string[a] != ' ' && a < len;++a) // Checking the string if the string is not a space, if it is a space, no instruction will be executed, only ++a and then repeats
		{
			b = string[a]; // Converting a character into Decimal (Look ASCII Table if you don't know the number codes!)
			
			// START Operator Checker
			if((b >= 48 && b <= 57)) // 1st - Number Checker with English Numeric Notation
			{
				if(string[a+1] == 's' && string[a+2] == 't') // Checks English Numeric Notation 
				{
					printf("%c%c", string[a], string[a+1]);
					++a;
				}
				else
				{
					if(string[a+1] == 'n' && string[a+2] == 'd')
					{
						printf("%c%c", string[a], string[a+1]);
						++a;
					}
					else
					{
						if(string[a+1] == 'r' && string[a+2] == 'd')
						{
							printf("%c%c", string[a], string[a+1]);
							++a;		
						}
						else
						{
							if(string[a+1] == 't' && string[a+2] == 'h')
							{
								printf("%c%c", string[a], string[a+1]);
								++a;			
							}
							else
							{
								if(string[a+1] == '.' || string[a+1] == ',')
								{
									printf("%c%c",string[a],string[a+1]);
									++a;
								}
								else
								{
									printf("%c",string[a]);
								}}}}}
			}
			else
			{
				if(string[a]==' ')
						{++a;
					}
					else {
				if((b >= 33 && b <= 47) || (b >= 58 && b <= 64) || (b >= 91 && b <= 96) || (b >= 123 && b <= 126)) // Special Chars checker
				{
					printf("\n%c\n",string[a]);

				}
				else
				{
					if(b >= 65 && b <= 90 || b >= 97 && b <= 122) // Letters Checker
					{
						printf("%c",string[a]);
					}
						else
						{
							printf("ERROR! Unrecognized string has been inserted!\n\n");
						}
			 		}
				}
			}
		}
		printf("\n");
	}
}

